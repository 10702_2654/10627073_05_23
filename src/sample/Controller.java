package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.IntStream;

public class Controller implements Initializable {
    public Button btnWorking;
    public ImageView IV;
    public ProgressBar pbWorking;
    public Timeline timeline;

    public void onWorking(ActionEvent actionEvent) {
        pbWorking.setVisible(true);
        timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);

        IntStream.range(1, 8).forEach(e -> {
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(e + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            IV.setImage(new Image(getClass().getResource("/動畫/".concat(String.valueOf(e)).concat(".jpg")).toString()));
                        }
                    })
            );
        });
        Task task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                final int max = 100;
                for (int i = 1; i < max; i++) {
                    Thread.sleep(100);
                    updateProgress(i, max);

                }
                return null;
            }
            @Override
            protected void scheduled() {
                super.scheduled();
                timeline.play();
                pbWorking.setVisible(true);
            }

            @Override
            protected void succeeded() {
                super.succeeded();
                pbWorking.setVisible(false);
                IV.setVisible(false);

            }

            @Override
            protected void failed() {
                super.failed();
            }
        };
        pbWorking.progressProperty().bind(task.progressProperty());//把工具的跑條綁到task的updateProgress
        pbWorking.setVisible(true);
        new Thread(task).start();

        /*List<String> imagepath = new ArrayList<>();
        for (int i = 0; i < 8; i++) {
            imagepath.add("/動畫/".concat(String.valueOf(i)).concat(".jpg"));
        }*/  //對應76行的呼叫

        /*Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.setAutoReverse(false);*/

        /*for (int i = 0; i < 8; i++) {
            int j = i;
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(i + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            //IV.setImage(new Image(getClass().getResource(imagepath.get(j)).toString()));
                            IV.setImage(new Image(getClass().getResource("/動畫/".concat(String.valueOf(j)).concat(".jpg")).toString()));
                        }
                    })
            );
        }*/ //等同下面的作法 for迴圈不同寫法
        /*IntStream.range(0, 8).forEach(e -> {
            timeline.getKeyFrames().add(
                    new KeyFrame(Duration.seconds(e + 1), new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            IV.setImage(new Image(getClass().getResource("/動畫/".concat(String.valueOf(e)).concat(".jpg")).toString()));
                        }
                    })
            );
        });
        timeline.play();*/

    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //pbWorking.setProgress(ProgressBar.INDETERMINATE_PROGRESS);//讓他跑條
        //pbWorking.setVisible(false);//一開始先關閉ProgressBar
        /*File file1 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\1.jpg");
        File file2 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\2.jpg");
        File file3 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\3.jpg");
        File file4 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\4.jpg");
        File file5 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\5.jpg");
        File file6 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\6.jpg");
        File file7 = new File("C:\\Users\\user\\IdeaProjects\\JavaFx期末專題\\動畫\\7.jpg");*/
        //IV.setImage(new Image(getClass().getResource("/動畫/1.jpg").toString()));

    }
}
